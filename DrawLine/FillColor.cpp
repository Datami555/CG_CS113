#include "FillColor.h"
#include <iostream>
#include <stack>
using namespace std;


SDL_Color getPixelColor(SDL_Surface* surface, int x, int y)
{
	Uint8 red, green, blue, alpha;

	//If the surface must be locked
	if (SDL_MUSTLOCK(surface))
	{
		//Lock the surface 
		SDL_LockSurface(surface);
	}

	//Convert the pixels to 32 bit
	Uint32 *pixels = (Uint32 *)surface->pixels;
	//Get the requested pixel
	Uint32 pixel = pixels[(y * surface->w) + x];

	//Get color of that pixel
	SDL_GetRGBA(pixel, surface->format, &red, &green, &blue, &alpha);

	//If the surface must be locked
	if (SDL_MUSTLOCK(surface))
	{
		//Lock the surface 
		SDL_UnlockSurface(surface);
	}

	SDL_Color color = { red, green, blue, alpha };
	return color;
}

//Compare two colors
bool compareTwoColors(SDL_Color color1, SDL_Color color2)
{
	if (color1.r == color2.r && color1.g == color2.g && color1.b == color2.b && color1.a == color2.a)
		return true;
	return false;
}

void BoundaryFill4(SDL_Window *win, Vector2D startPoint, Uint32 pixel_format,
	SDL_Renderer *ren, SDL_Color fillColor, SDL_Color boundaryColor)
{
	SDL_Color preColor = SDL_Color();
	SDL_GetRenderDrawColor(ren, &preColor.r, &preColor.g, &preColor.b, &preColor.a);

	SDL_Surface* surface = SDL_GetWindowSurface(win);
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	
	int xc, yc;
	xc = startPoint.x;
	yc = startPoint.y;
	
	int w, h;
	SDL_GetRendererOutputSize(ren, &w, &h);

	stack<pair<int, int>> xPool;
	xPool.push(make_pair(xc, yc));

	while (xPool.empty() == false)
	{
		pair<int, int> point = xPool.top();
		xPool.pop();

		xc = point.first;
		yc = point.second;

		if (xc > 0 && xc < w && yc > 0 && yc < h)
		{
			SDL_Color color = getPixelColor(surface, xc, yc);

			if ((compareTwoColors(color, fillColor) == false) && (compareTwoColors(color, boundaryColor) == false))
			{
				SDL_RenderDrawPoint(ren, xc, yc);

				xPool.push(make_pair(xc - 1, yc));
				xPool.push(make_pair(xc + 1, yc));
				xPool.push(make_pair(xc, yc - 1));
				xPool.push(make_pair(xc, yc + 1));
			}
		}
	}

	SDL_SetRenderDrawColor(ren, preColor.r, preColor.g, preColor.b, preColor.a);
}

//======================================================================================================================
//=============================================FILLING TRIANGLE=========================================================

int maxIn3(int a, int b, int c)
{
	if (a > b)
	{
		if (a > c)
		{
			return a;
		}
		else
		{
			return c;
		}
	}
	else
	{
		if (b > c)
		{
			return b;
		}
		else
		{
			return c;
		}
	}
	
}

int minIn3(int a, int b, int c)
{
	if (a < b)
	{
		if (a < c)
		{
			return a;
		}
		else
		{
			return c;
		}
	}
	else
	{
		if (b < c)
		{
			return b;
		}
		else
		{
			return c;
		}
	}
}

void swap(Vector2D &a, Vector2D &b)
{
	Vector2D c = a;
	a = b;
	b = c;

	//a.x += b.x;
	//b.x = a.x - b.x;
	//a.x -= b.x;

	//a.y += b.y;
	//b.y = a.y - b.y;
	//a.y -= b.y;
}

void ascendingSort(Vector2D &v1, Vector2D &v2, Vector2D &v3)
{
	//Respect to y;
	
	if (v1.y > v2.y)
	{
		swap(v1, v2);
	}
	if (v2.y > v3.y)
	{
		swap(v2, v3);
	}
	if (v1.y > v3.y)
	{
		swap(v1, v3);
	}
}

void TriangleFill1(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);

	//v1.y == v2.y == v3.y
	int xl = minIn3(v1.x, v2.x, v3.x);
	int xr = maxIn3(v1.x, v2.x, v3.x);
	Bresenham_Line(xl, v1.y, xr, v3.y, ren);
}

void TriangleFill2(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);

	//v1.y < (v2.y == v3.y)
	//		  y1
	//		 *  *
	//		******
	float dm12, dm13;
	float x2, x3;
	int yScan;

	dm12 = (float)(v2.x - v1.x) / (v2.y - v1.y);
	dm13 = (float)(v3.x - v1.x) / (v3.y - v1.y);

	x2 = v2.x;
	x3 = v3.x;
	yScan = v2.y;

	Bresenham_Line(x2, yScan, x3, yScan, ren);
	//cout << "(" << x2 << ":" << yScan << ") (" << x3 << ":" << yScan << ")\n";

	while (yScan >= v1.y)
	{
		x2 = x2 - dm12;
		x3 = x3 - dm13;
		yScan--;

		//cout << "(" << x2 << ":" << yScan << ") (" << x3 << ":" << yScan << ")\n";
		Bresenham_Line(int(x2+ 0.5), yScan, int(x3 + 0.5), yScan, ren);
	}
}

void TriangleFill3(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);

	//v3.y > (v1.y == v2.y)
	//		******
	//		 *  *
	//		  y3
	float dm31, dm32;
	float x1, x2;
	int yScan;

	dm31 = (float)(v3.x - v1.x) / (v3.y - v1.y);
	dm32 = (float)(v3.x - v2.x) / (v3.y - v2.y);

	x1 = v1.x;
	x2 = v2.x;
	yScan = v1.y;
	Bresenham_Line(x1, yScan, x2, yScan, ren);

	while (yScan <= v3.y)
	{
		x1 = x1 + dm31;
		x2 = x2 + dm32;
		yScan++;

		Bresenham_Line(int(x1 + 0.5), yScan, int(x2 + 0.5), yScan, ren);
	}
}

void TriangleFill4(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	//v1.y < v2.y < v3.y
	//		   y1
	//		  *  *
	//		y2	  y4
	//          *  *
	//			   y3

	Vector2D v13 = Vector2D(v1).sub(v3);

	Vector2D v4 = Vector2D();
	v4.y = v2.y;
	v4.x = v1.x + v13.x * (v4.y - v1.y) / v13.y;

	TriangleFill2(v1, v2, v4, ren, fillColor);
	TriangleFill3(v2, v4, v3, ren, fillColor);
}

void TriangleFill(Vector2D v1, Vector2D v2, Vector2D v3, SDL_Renderer *ren, SDL_Color fillColor)
{
	SDL_Color preColor = SDL_Color();
	SDL_GetRenderDrawColor(ren, &preColor.r, &preColor.g, &preColor.b, &preColor.a);

	ascendingSort(v1, v2, v3);

	if (v1.y == v2.y == v3.y)
	{
		TriangleFill1(v1, v2, v3, ren, fillColor);
	}
	else if ((v2.y == v3.y) && (v2.y > v1.y))
	{
		TriangleFill2(v1, v2, v3, ren, fillColor);
	}
	else if ((v1.y == v2.y) && (v1.y < v3.y))
	{
		TriangleFill3(v1, v2, v3, ren, fillColor);
	}
	else
	{
		TriangleFill4(v1, v2, v3, ren, fillColor);
	}
	SDL_SetRenderDrawColor(ren, preColor.r, preColor.g, preColor.b, preColor.a);
}

//======================================================================================================================
//===================================CIRCLE - RECTANGLE - ELLIPSE=======================================================
bool isInsideCircle(int xc, int yc, int R, int x, int y)
{
	if (((x - xc)*(x - xc) + (y - yc)*(y - yc)) < R*R)
	{
		return true;
	}
	return false;
}

int minIn2(int a, int b)
{
	if (a < b)
	{
		return a;
	}
	return b;
}

int maxIn2(int a, int b)
{
	if (a > b)
	{
		return a;
	}
	return b;
}

void FillIntersection(int x1, int y1, int x2, int y2, int xc, int yc, int R,
	SDL_Renderer *ren, SDL_Color fillColor)
{
	int xl = minIn2(x1, x2);
	int xr = maxIn2(x1, x2);

	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	for (int x = xl; x <= xr; x++)
	{
		if (isInsideCircle(xc, yc, R, x, y1))
		{
			SDL_RenderDrawPoint(ren, x, y1);
		}
	}
}

void FillIntersectionRectangleCircle(Vector2D vTopLeft, Vector2D vBottomRight, int xc, int yc, int R,
	SDL_Renderer *ren, SDL_Color fillColor)
{
	SDL_Color preColor = SDL_Color();
	SDL_GetRenderDrawColor(ren, &preColor.r, &preColor.g, &preColor.b, &preColor.a);

	int xl = minIn2(vTopLeft.x, vBottomRight.x);
	int xr = maxIn2(vTopLeft.x, vBottomRight.x);
	int yt = maxIn2(vTopLeft.y, vBottomRight.y);
	int yb = minIn2(vTopLeft.y, vBottomRight.y);

	for (int y = yb; y < yt; y++)
	{
		FillIntersection(xl, y, xr, y, xc, yc, R, ren, fillColor);
	}

	SDL_SetRenderDrawColor(ren, preColor.r, preColor.g, preColor.b, preColor.a);
}

void RectangleFill(Vector2D vTopLeft, Vector2D vBottomRight, SDL_Renderer *ren, SDL_Color fillColor)
{
	SDL_Color preColor = SDL_Color();
	SDL_GetRenderDrawColor(ren, &preColor.r, &preColor.g, &preColor.b, &preColor.a);

	int xl = minIn2(vTopLeft.x, vBottomRight.x);
	int xr = maxIn2(vTopLeft.x, vBottomRight.x);
	
	SDL_SetRenderDrawColor(ren, fillColor.r, fillColor.g, fillColor.b, fillColor.a);
	for (int y = vTopLeft.y; y < vBottomRight.y; y++)
	{
		for (int x = xl; x < xr; x++)
		{
			SDL_RenderDrawPoint(ren, x, y);
		}

	}

	SDL_SetRenderDrawColor(ren, preColor.r, preColor.g, preColor.b, preColor.a);
}

void put4line(int xc, int yc, int x, int y, SDL_Renderer *ren, SDL_Color fillColor)
{
	//lower horizontal
	SDL_RenderDrawLine(ren, xc + x, yc + y, xc - x, yc + y);
	//upper horizontal
	SDL_RenderDrawLine(ren, xc + x, yc - y, xc - x, yc - y);
	//righty vertical
	SDL_RenderDrawLine(ren, xc + y, yc + x, xc + y, yc - x);
	//lefty vertical
	SDL_RenderDrawLine(ren, xc - y, yc + x, xc - y, yc - x);
}

void CircleFill(int xc, int yc, int R, SDL_Renderer *ren, SDL_Color fillColor)
{
	SDL_Color preColor = SDL_Color();
	SDL_GetRenderDrawColor(ren, &preColor.r, &preColor.g, &preColor.b, &preColor.a);

	int x = R;
	int y = 0;
	int p = 3 - (R << 1);

	put4line(xc, yc, x, y, ren, fillColor);

	while (x > y)
	{
		if (p <= 0)
		{
			p += (y << 2) + 6;
		}
		else
		{
			p += ((y - x) << 2) + 10;
			x--;
		}
		y++;

		put4line(xc, yc, x, y, ren, fillColor);
	}

	SDL_SetRenderDrawColor(ren, preColor.r, preColor.g, preColor.b, preColor.a);
}

void put2line(int xc, int yc, int x, int y, int xcR, int ycR, int R, SDL_Renderer *ren, SDL_Color fillColor)
{
	//lower horizontal
	FillIntersection(xc + x, yc + y, xc - x, yc + y, xcR, ycR, R, ren, fillColor);
	//upper horizontal
	FillIntersection(xc + x, yc - y, xc - x, yc - y, xcR, ycR, R, ren, fillColor);
}

void FillIntersectionEllipseCircle(int xcE, int ycE, int a, int b, int xc, int yc, int R,
	SDL_Renderer *ren, SDL_Color fillColor)
{
	SDL_Color preColor = SDL_Color();
	SDL_GetRenderDrawColor(ren, &preColor.r, &preColor.g, &preColor.b, &preColor.a);
	//Draw first area of 1/4 Ellipse (dx > 0, dy < 0, -1 < y' < 0)
	int x = 0;
	int y = b;
	int a2 = a*a;
	int a4 = a2*a2;
	int b2 = b*b;
	int p = 2 * b2 + a2*(1 - 2 * b);
	int const1 = 4 * b2;
	int const2 = 6 * b2;
	int const3 = 4 * a2;

	put2line(xcE, ycE, x, y, xc, yc, R, ren, fillColor);

	while ((a2 + b2)*x*x <= a4)
	{
		if (p <= 0)
		{
			p += const1*x + const2;
		}
		else
		{
			p += const3*(1 - y) + const1*x + const2;
			y--;
		}

		x++;

		put2line(xcE, ycE, x, y, xc, yc, R, ren, fillColor);
	}

	//Draw second area of 1/4 Ellipse (dx < 0, dy > 0, y' < -1)
	x = a;
	y = 0;
	p = 2 * a2 + b2*(1 - 2 * a);
	const1 = 4 * a2;
	const2 = 6 * a2;
	const3 = 4 * b2;

	put2line(xcE, ycE, x, y, xc, yc, R, ren, fillColor);

	while ((a2 + b2)*x*x >= a4)
	{
		if (p <= 0)
		{
			p += const1*y + const2;
		}
		else
		{
			p += const3*(1 - x) + const1*y + const2;
			x--;
		}

		y++;

		put2line(xcE, ycE, x, y, xc, yc, R, ren, fillColor);
	}

	SDL_SetRenderDrawColor(ren, preColor.r, preColor.g, preColor.b, preColor.a);
}

void put4line(int xc, int yc, int x, int y, int xcR, int ycR, int R, SDL_Renderer *ren, SDL_Color fillColor)
{
	//lower horizontal
	FillIntersection(xc + x, yc + y, xc - x, yc + y, xcR, ycR, R, ren, fillColor);
	//upper horizontal
	FillIntersection(xc + x, yc - y, xc - x, yc - y, xcR, ycR, R, ren, fillColor);
	//righty vertical
	FillIntersection(xc + y, yc + x, xc + y, yc - x, xcR, ycR, R, ren, fillColor);
	//lefty vertical
	FillIntersection(xc - y, yc + x, xc - y, yc - x, xcR, ycR, R, ren, fillColor);
}

void FillIntersectionTwoCircles(int xc1, int yc1, int R1, int xc2, int yc2, int R2,
	SDL_Renderer *ren, SDL_Color fillColor)
{
	SDL_Color preColor = SDL_Color();
	SDL_GetRenderDrawColor(ren, &preColor.r, &preColor.g, &preColor.b, &preColor.a);

	int x = R1;
	int y = 0;
	int p = 3 - (R1 << 1);

	put4line(xc1, yc1, x, y, xc2, yc2, R2, ren, fillColor);

	while (x > y)
	{
		if (p <= 0)
		{
			p += (y << 2) + 6;
		}
		else
		{
			p += ((y - x) << 2) + 10;
			x--;
		}
		y++;

		put4line(xc1, yc1, x, y, xc2, yc2, R2, ren, fillColor);
	}

	SDL_SetRenderDrawColor(ren, preColor.r, preColor.g, preColor.b, preColor.a);
}