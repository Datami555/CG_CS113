#include "Bezier.h"
#include <iostream>
#include <queue>
using namespace std;


void DrawCurve(SDL_Renderer *ren, SDL_Point *poolPoints, int n)
{
	queue<SDL_Point> pool;

	for (int i = 0; i < n; i++)
	{
		pool.push(poolPoints[n]);
	}

	for (int t = 0; t < 1; t+= 0.001)
	{
		for (int times = n - 1; times > 0; times--)
		{
			for (int i = 0; i < times; i++)
			{
				SDL_Point p1 = pool.front(); //err, cannot get SDL_Point by dequeue
				SDL_Point p2 = pool.front();
				pool.pop();

				SDL_Point p0;
				p0.x = (1 - t)*p1.x + t*p2.x;
				p0.y = (1 - t)*p1.y + t*p2.y;
				pool.push(p0);
			}
			pool.pop();
		}
		SDL_Point drawPoint = pool.front();
		pool.pop();
	}
		
}

void DrawCurve2(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3)
{
	for (float t = 0; t < 1; t+= 0.0001)
	{
		float t2 = t*t;
		
		float a0 = t2 - 2 * t + 1;					  
		float a1 = 2 * t - 2 * t2;
		float a2 = t2;

		int px = int(a0*p1.x + a1*p2.x + a2*p3.x + 0.5);
		int py = int(a0*p1.y + a1*p2.y + a2*p3.y + 0.5);

		SDL_RenderDrawPoint(ren, px, py);
	}
}

void DrawCurve3(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3, Vector2D p4)
{
	for (float t = 0; t < 1; t += 0.0001)
	{
		float t2 = t*t;
		float t3 = t2*t;

		float a0 = -t3 + 3 * t2 - 3 * t + 1;
		float a1 = 3 * t3 - 6 * t2 + 3 * t;
		float a2 = 3 * t2 - 3 * t3;
		float a3 = t3;

		int px = int(a0*p1.x + a1*p2.x + a2*p3.x + a3*p4.x + 0.5);
		int py = int(a0*p1.y + a1*p2.y + a2*p3.y + a3*p4.y + 0.5);

		SDL_RenderDrawPoint(ren, px, py);
	}
}


