#include "Parabol.h"

void Draw2Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	int new_x;
	int new_y;

	new_x = xc + x;
	new_y = yc + y;
	SDL_RenderDrawPoint(ren, new_x, new_y);

	new_x = xc - x;
	new_y = yc + y;
	SDL_RenderDrawPoint(ren, new_x, new_y);
}

void BresenhamDrawParabolPositive(int xc, int yc, int A, SDL_Renderer *ren)
{
	//Area 1
	int x = 0;
	int y = 0;
	int p = 1 - A;
	int xMax, yMax;
	SDL_GetRendererOutputSize(ren, &xMax, &yMax);

	Draw2Points(xc, yc, x, y, ren);

	while (x < A)
	{
		if (p <= 0)
		{
			p += 2 * x + 3;
		}
		else
		{
			p += 2 * x + 3 - 2 * A;
			y++;
		}
		x++;
		Draw2Points(xc, yc, x, y, ren);
	}

	//Area 2
	x = A;
	y = A / 2;
	p = 2 * A - 1;

	Draw2Points(xc, yc, x, y, ren);

	while (y < yMax)
	{
		if (p <= 0)
		{
			p += 4 * A;
		}
		else
		{
			p += 4 * A - 4 * x - 4;
			x++;
		}
		y++;
		Draw2Points(xc, yc, x, y, ren);
	}
}

void BresenhamDrawParabolNegative(int xc, int yc, int A, SDL_Renderer *ren)
{
	//Area 1
	int x = 0;
	int y = 0;
	int p = 1 - A;
	int xMax, yMax;
	SDL_GetRendererOutputSize(ren, &xMax, &yMax);

	Draw2Points(xc, yc, x, y, ren);

	while (x < A)
	{
		if (p <= 0)
		{
			p += 2 * x + 3;
		}
		else
		{
			p += 2 * x + 3 - 2 * A;
			y--;
		}
		x++;
		Draw2Points(xc, yc, x, y, ren);
	}

	//Area 2
	x = A;
	y = -A / 2;
	p = 2 * A - 1;

	Draw2Points(xc, yc, x, y, ren);

	while (y > -yMax)
	{
		if (p <= 0)
		{
			p += 4 * A;
		}
		else
		{
			p += 4 * A - 4 * x - 4;
			x++;
		}
		y--;
		Draw2Points(xc, yc, x, y, ren);
	}
}
