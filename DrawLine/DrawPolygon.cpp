#include "DrawPolygon.h"
#include <iostream>
#include <cmath>

using namespace std;

void DrawEquilateralTriangle(int xc, int yc, int R, SDL_Renderer *ren)
{
	double phi = M_PI / 2;
	double delta = 2 * M_PI / 3;
	int nV = 3;
	int x[3], y[3];

	//calculate the coordinate of vertexes
	for (int i = 0; i < nV; i++)
	{
		x[i] = xc + int(R * cos(phi) + 0.5);
		y[i] = yc - int(R * sin(phi) + 0.5);

		phi += delta;
	}

	//draw 
	for (int i = 0; i < nV; i++)
	{
		Bresenham_Line(x[i], y[i], x[(i + 1) % nV], y[(i + 1) % nV], ren);
	}
}

void DrawSquare(int xc, int yc, int R, SDL_Renderer *ren)
{
	double phi = M_PI / 4;
	double delta = 2 * M_PI / 4;
	int nV = 4;
	int x[4], y[4];

	//calculate the coordinate of vertexes
	for (int i = 0; i < nV; i++)
	{
		x[i] = xc + int(R * cos(phi) + 0.5);
		y[i] = yc - int(R * sin(phi) + 0.5);

		phi += delta;
	}

	//draw 
	for (int i = 0; i < nV; i++)
	{
		Bresenham_Line(x[i], y[i], x[(i + 1) % nV], y[(i + 1) % nV], ren);
	}
}
void DrawPentagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	double phi = M_PI / 2;
	double delta = 2 * M_PI / 5;
	int nV = 5;
	int x[5], y[5];

	//calculate the coordinate of vertexes
	for (int i = 0; i < nV; i++)
	{
		x[i] = xc + int(R * cos(phi) + 0.5);
		y[i] = yc - int(R * sin(phi) + 0.5);

		phi += delta;
	}

	//draw 
	for (int i = 0; i < nV; i++)
	{
		Bresenham_Line(x[i], y[i], x[(i + 1) % nV], y[(i + 1) % nV], ren);
	}
}
void DrawHexagon(int xc, int yc, int R, SDL_Renderer *ren)
{
	double phi = M_PI / 2;
	double delta = 2 * M_PI / 6;
	int nV = 6;
	int x[6], y[6];

	//calculate the coordinate of vertexes
	for (int i = 0; i < nV; i++)
	{
		x[i] = xc + int(R * cos(phi) + 0.5);
		y[i] = yc - int(R * sin(phi) + 0.5);

		phi += delta;
	}

	//draw 
	for (int i = 0; i < nV; i++)
	{
		Bresenham_Line(x[i], y[i], x[(i + 1) % nV], y[(i + 1) % nV], ren);
	}
}

void DrawStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	double phi = M_PI / 2;
	double delta = 2 * M_PI / 5;
	int nV = 5;
	int x[5], y[5];

	//calculate the coordinate of vertexes
	for (int i = 0; i < nV; i++)
	{
		x[i] = xc + int(R * cos(phi) + 0.5);
		y[i] = yc - int(R * sin(phi) + 0.5);

		phi += delta;
	}

	//draw 
	for (int i = 0; i < nV; i++)
	{
		Bresenham_Line(x[i], y[i], x[(i + 2) % nV], y[(i + 2) % nV], ren);
	}
}

void DrawEmptyStar(int xc, int yc, int R, SDL_Renderer *ren)
{
	double phi = M_PI / 2;
	double delta = M_PI / 5; //for small petagon inside the star

	int nV = 5;
	int r = int(R*sin(M_PI / 10) / sin(7 * M_PI / 10) + 0.5);
	int x[5], y[5];
	int xp[5], yp[5];

	for (int i = 0; i < nV; i++)
	{
		//calculate the coordinate of large petagon
		x[i] = xc + int(R * cos(phi) + 0.5);
		y[i] = yc - int(R * sin(phi) + 0.5);
		
		//calculate the coordinate of small petagon
		phi += delta;
		xp[i] = xc + int(r * cos(phi) + 0.5);
		yp[i] = yc - int(r * sin(phi) + 0.5);
		
		//next to next vertex of large petagon
		phi += delta;
	}

	//draw 
	for (int i = 0; i < nV; i++)
	{
		Bresenham_Line(x[i], y[i], xp[i], yp[i], ren);
		Bresenham_Line(xp[i], yp[i], x[(i + 1) % nV], y[(i + 1) % nV], ren);
	}
}

//Star with eight wings
void DrawStarEight(int xc, int yc, int R, SDL_Renderer *ren)
{
	double phi = M_PI / 2;
	double delta = M_PI / 8;
	int r = int(R*sin(M_PI / 8) / sin(5 * M_PI / 8) + 0.5);
	int nV = 8;
	int x[8], y[8];
	int xp[8], yp[8];


	//calculate the coordinate of vertexes
	for (int i = 0; i < nV; i++)
	{
		x[i] = xc + int(R * cos(phi) + 0.5);
		y[i] = yc - int(R * sin(phi) + 0.5);

		phi += delta;
		xp[i] = xc + int(r * cos(phi) + 0.5);
		yp[i] = yc - int(r * sin(phi) + 0.5);

		phi += delta;
	}

	//draw 
	for (int i = 0; i < nV; i++)
	{
		Bresenham_Line(x[i], y[i], xp[i], yp[i], ren);
		Bresenham_Line(xp[i], yp[i], x[(i + 1) % nV], y[(i + 1) % nV], ren);
	}
}

//For drawing one star of convergent star
void DrawStarAngle(int xc, int yc, int R, double startAngle, SDL_Renderer *ren)
{
	double phi = startAngle;
	double delta = M_PI / 5; //for small petagon inside the star

	int nV = 5;
	int r = int(R*sin(M_PI / 10) / sin(7 * M_PI / 10) + 0.5);
	int x[5], y[5];
	int xp[5], yp[5];

	for (int i = 0; i < nV; i++)
	{
		//calculate the coordinate of large petagon
		x[i] = xc + int(R * cos(phi) + 0.5);
		y[i] = yc - int(R * sin(phi) + 0.5);

		//calculate the coordinate of small petagon
		phi += delta;
		xp[i] = xc + int(r * cos(phi) + 0.5);
		yp[i] = yc - int(r * sin(phi) + 0.5);

		//next to next vertex of large petagon
		phi += delta;
	}

	//draw 
	for (int i = 0; i < nV; i++)
	{
		Bresenham_Line(x[i], y[i], xp[i], yp[i], ren);
		Bresenham_Line(xp[i], yp[i], x[(i + 1) % nV], y[(i + 1) % nV], ren);
	}
}

void DrawConvergentStar(int xc, int yc, int r, SDL_Renderer *ren)
{
	double phi = M_PI / 2;
	int R = r;
	
	while (R >= 1)
	{
		DrawStarAngle(xc, yc, R, phi, ren);

		R = int(R*sin(M_PI / 10) / sin(7 * M_PI / 10) + 0.5);

		phi += M_PI;
	}
}
